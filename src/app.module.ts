import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostAModule } from './modules/post-a/post-a.module';

@Module({
  imports: [PostAModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
