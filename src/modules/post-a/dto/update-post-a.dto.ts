import { PartialType } from '@nestjs/mapped-types';
import { CreatePostADto } from './create-post-a.dto';

export class UpdatePostADto extends PartialType(CreatePostADto) {}
