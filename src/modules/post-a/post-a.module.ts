import { Module } from '@nestjs/common';
import { PostAService } from './post-a.service';
import { PostAController } from './post-a.controller';

@Module({
  controllers: [PostAController],
  providers: [PostAService]
})
export class PostAModule {}
