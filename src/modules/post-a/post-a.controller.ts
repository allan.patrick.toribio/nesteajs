import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PostAService } from './post-a.service';
import { CreatePostADto } from './dto/create-post-a.dto';
import { UpdatePostADto } from './dto/update-post-a.dto';

@Controller('post-a')
export class PostAController {
  constructor(private readonly postAService: PostAService) {}

  @Post()
  create(@Body() createPostADto: CreatePostADto) {
    return this.postAService.create(createPostADto);
  }

  @Get()
  findAll() {
    return this.postAService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postAService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePostADto: UpdatePostADto) {
    return this.postAService.update(+id, updatePostADto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.postAService.remove(+id);
  }
}
