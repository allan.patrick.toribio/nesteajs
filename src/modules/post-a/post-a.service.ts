import { Injectable } from '@nestjs/common';
import { CreatePostADto } from './dto/create-post-a.dto';
import { UpdatePostADto } from './dto/update-post-a.dto';

@Injectable()
export class PostAService {
  create(createPostADto: CreatePostADto) {
    return 'This action adds a new postA';
  }

  findAll() {
    return `This action returns all postA`;
  }

  findOne(id: number) {
    return `This action returns a #${id} postA`;
  }

  update(id: number, updatePostADto: UpdatePostADto) {
    return `This action updates a #${id} postA`;
  }

  remove(id: number) {
    return `This action removes a #${id} postA`;
  }
}
