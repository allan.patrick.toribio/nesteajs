import { Test, TestingModule } from '@nestjs/testing';
import { PostAController } from './post-a.controller';
import { PostAService } from './post-a.service';

describe('PostAController', () => {
  let controller: PostAController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostAController],
      providers: [PostAService],
    }).compile();

    controller = module.get<PostAController>(PostAController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
