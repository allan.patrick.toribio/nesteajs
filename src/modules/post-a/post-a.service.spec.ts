import { Test, TestingModule } from '@nestjs/testing';
import { PostAService } from './post-a.service';

describe('PostAService', () => {
  let service: PostAService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostAService],
    }).compile();

    service = module.get<PostAService>(PostAService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
